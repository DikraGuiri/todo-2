<?php
// Database connection parameters.
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'todolist');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3306');

// Connect to MySQL
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// Check connection
if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}

// Retrieve tasks from the database
$sql = "SELECT * FROM todo ORDER BY created_at DESC";
$result = $mysqli->query($sql);

$taches = [];
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $taches[] = $row;
    }
}

// Handle POST requests
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['action'])) {
        $action = $_POST['action'];
        $taskId = $_POST['id'];

        if ($action === 'new') {
            $newTask = $_POST['title'];
            $sql = "INSERT INTO todo (title) VALUES ('$newTask')";
            $mysqli->query($sql);
        } elseif ($action === 'delete') {
            $sql = "DELETE FROM todo WHERE id=$taskId";
            $mysqli->query($sql);
        } elseif ($action === 'toggle') {
            $sql = "UPDATE todo SET done = 1 - done WHERE id=$taskId";
            $mysqli->query($sql);
        }
        header("Location: index.php");
        exit();
    }
}
?>

